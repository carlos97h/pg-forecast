# Database Mock

## Table of Contents

- [Overview](#overview)
- [Stack](#stack)
- [Requirements](#requirements)
- [How to Use](#how-to-use)
  * [Mock without using this image](#mock-the-database-without-using-this-image)

## Overview

This mock is used to develop locally, to avoid connecting to the production database.
  
## Stack

* [MongoDB](https://www.mongodb.com/)

## Requirements

To use this mock with more options, you can install [MongoDB Compass](https://www.mongodb.com/products/compass) and
[MongoDB Database Tools](https://www.mongodb.com/try/download/database-tools).

It uses mongo 3.6, but any latter version is compatible.

## How to use

First, you'll need to build the image.

```bash
docker build . -t paragliding-mongo
```

This will do everything needed for the database to have the locations loaded. Then, you only need to run it.

```bash
docker run -p 27017:27017 --name paragliding-database -d paragliding-mongo
```

## Mock the database without using this image

If you decided that you don't want to use this image, and instead are using a mongo image or mongo community, you can simply
import all the locations to your database. Simply run the following command.

```bash
mongoimport -d data -c Locations --file resources/locations.json --jsonArray
```

Note: You may need to install [MongoDB Database Tools](https://www.mongodb.com/try/download/database-tools).

You can also add you own location by modifying the `locations.json` file
import {configureStore} from '@reduxjs/toolkit';
import modelsForecast from './reducers/models-forecast';

export default configureStore({
    reducer: {
        modelsForecast: modelsForecast,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}),
})
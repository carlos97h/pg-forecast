import React from "react";

import "./loading-spinner.css"

const LoadingSpinner: React.FunctionComponent<{}> = () => {
    return (
        <div className="loader-container">
            <div className="loader"></div>
        </div>
    )
}

export default LoadingSpinner;
import React from "react";
import './forecast-table.module.css'
import {LocationForecast} from "../../../../dto/LocationForecast";
import ForecastRow from "../forecast-row/forecast-row";
import styles from "./forecast-table.module.css";

interface IProps {
    locationsForecast: LocationForecast[];
}

const ForecastTable: React.FunctionComponent<IProps> = (props) => {
    const {
        locationsForecast = [],
    } = props;


    return (
        <div className={styles.forecastsHolder}>
            {
                locationsForecast.map((locationForecast) => {
                    return (
                        <ForecastRow key={locationForecast.location.name} locationForecast={locationForecast}/>
                    )
                })
            }
        </div>
    );
}

export default ForecastTable;
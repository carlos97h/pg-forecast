import {LocationForecast} from "../../../../dto/LocationForecast";
import React, {useState} from "react";
import ForecastLocation from "../forecast-location/forecast-location";
import TableElement from "../table-element/table-element";
import styles from './forecast-row.module.css'

interface IProps {
    locationForecast: LocationForecast;
}

const ForecastRow: React.FunctionComponent<IProps> = (props) => {
    const {
        locationForecast
    } = props;

    const [expanded, setExpanded] = useState<boolean>(false);

    const toggleAllField = () => setExpanded(!expanded);

    return (
        <div className={styles.forecast} key={locationForecast.location.name}>

            <ForecastLocation expanded={expanded} toggle={toggleAllField} location={locationForecast.location}/>

            <div className={styles.halfSideBorder}/>

            <div className={styles.tableElementHolder}>
                <TableElement expanded={expanded} days={locationForecast.days}/>
            </div>

        </div>
    );
}

export default ForecastRow;
import React from "react";
import {ForecastDay} from "../../../../dto/LocationForecast";
import styles from "./table-day.module.css"
import WindDirection from "../../../wind-direction/wind-direction";
import commonStyles from "../../../common-styles.module.css";


interface IProps {
    day: ForecastDay
    expanded: boolean
}

const handleScore = (score: number | null) => {
    return score != null ? Math.trunc(score * 100) : null;
}

const TableDay: React.FunctionComponent<IProps> = (props) => {
    const {day, expanded} = props;

    const validHours = day.hours.filter(el => el.hour >= day.sunrise && el.hour <= day.sunset);

    return (
        <div className={styles.forecastDay}>

            <div className={styles.labelDayScore}>
                {day.date} | {handleScore(day.score) ?? "?"}
            </div>

            <ul className={styles.forecastHours}>

                {validHours.map(hour => (
                    <li className={styles.forecastHour} key={hour.hour}>

                        <span className={commonStyles.label}>{hour.hour.split(':')[0]}</span>
                        <span className={commonStyles.label}>{handleScore(hour.score) ?? "?"}</span>

                        {expanded && <>
                            <span className={commonStyles.label}>{hour.windSpeed ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.windGust ?? "?"}</span>
                            <span className={commonStyles.label}><WindDirection windDirection={hour.windDir}/></span>
                            <span className={commonStyles.label}>{hour.temp ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.precipitation ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.humidity ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.cloudCoverLow ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.cloudCoverMedium ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.cloudCoverHigh ?? "?"}</span>
                            <span className={commonStyles.label}>{hour.visibility ?? "?"}</span>
                        </>}

                    </li>
                ))}

            </ul>

        </div>
    );
}

export default TableDay;
import React from "react";
import styles from "./header.module.css";

const Header: React.FunctionComponent = () => {
    return (
        <header className={styles.headerHolder}>

            <div className={styles.logo}>
                Paragliding Forecast
            </div>

            <div className={styles.darkMode}>
                Not available in dark mode
            </div>

        </header>
    );
}

export default Header;
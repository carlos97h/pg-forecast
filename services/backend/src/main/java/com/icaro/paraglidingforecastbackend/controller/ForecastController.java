package com.icaro.paraglidingforecastbackend.controller;

import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.services.WeatherForecastService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("forecast")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost", "http://85.244.239.168", "http://192.168.1.133"})
public class ForecastController {

    final WeatherForecastService weatherForecastService;

    public ForecastController(final WeatherForecastService weatherForecastService) {
        this.weatherForecastService = weatherForecastService;
    }

    @GetMapping("/all")
    public Map<String, List<Forecast>> getForecastsByModel() {
        return weatherForecastService.getForecastsByModel();
    }
}

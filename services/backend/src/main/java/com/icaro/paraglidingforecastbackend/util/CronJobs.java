package com.icaro.paraglidingforecastbackend.util;

import com.icaro.paraglidingforecastbackend.repositories.LocationRepository;
import com.icaro.paraglidingforecastbackend.services.WeatherModelService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CronJobs {
    LocationRepository locationRepository;
    MongoTemplate mongoTemplate;
    List<WeatherModelService> modelServices;

    public CronJobs(
            LocationRepository locationRepository,
            MongoTemplate mongoTemplate,
            List<WeatherModelService> modelServices) {
        this.locationRepository = locationRepository;
        this.mongoTemplate = mongoTemplate;
        this.modelServices = modelServices;
    }

    @Scheduled(fixedDelay = 1000 * 60 * 60 * 12)
    public void fetchData() {
        // handle api data
        for (WeatherModelService model : modelServices) {
            // TODO: there's probably a better way to do this
            String collectionName = model.getClass().getName().split("WeatherModelService")[1];
            mongoTemplate.dropCollection(collectionName);
            mongoTemplate.insert(model.getForecasts(locationRepository.findAll()), collectionName);
        }
    }

}

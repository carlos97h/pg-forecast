package com.icaro.paraglidingforecastbackend.dto.WeatherApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class WeatherApiForecast {
  private final WeatherApiForecastDay[] days;

  public WeatherApiForecast(@JsonProperty("forecastday") final WeatherApiForecastDay[] days) {
    this.days = days;
  }
}

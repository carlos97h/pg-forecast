package com.icaro.paraglidingforecastbackend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.icaro.paraglidingforecastbackend.clients.VisualCrossingClient;
import feign.Feign;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Bean;

@ConstructorBinding
@ConfigurationProperties(prefix = "visual-crossing")
public class VisualCrossingClientConfiguration {
    private static final String VISUALCROSSING_URL = "https://weather.visualcrossing.com";

    private final String apiKey;

    public VisualCrossingClientConfiguration(String apiKey) {
        this.apiKey = apiKey;
    }

    @Bean
    public VisualCrossingClient visualCrossingClient() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());

        return Feign.builder()
                .encoder(new JacksonEncoder(objectMapper))
                .decoder(new JacksonDecoder(objectMapper))
                .requestInterceptor(new VisualCrossingInterceptor(apiKey))
                .target(VisualCrossingClient.class, VISUALCROSSING_URL);
    }

    private static class VisualCrossingInterceptor implements RequestInterceptor {
        private final String apiKey;

        public VisualCrossingInterceptor(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public void apply(RequestTemplate template) {
            final String uri = String.format("%s&key=%s", template.url().split(VISUALCROSSING_URL)[0], apiKey);
            template.uri(uri);
        }
    }

}

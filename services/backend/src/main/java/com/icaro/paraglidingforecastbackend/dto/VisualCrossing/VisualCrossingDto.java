package com.icaro.paraglidingforecastbackend.dto.VisualCrossing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.icaro.paraglidingforecastbackend.models.location.Coordinate;
import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class VisualCrossingDto {
  private final Coordinate coordinate;
  private final String timezone;
  private final Integer tzOffset;
  private final VisualCrossingDay[] days;

  public VisualCrossingDto(
      @JsonProperty("resolvedAddress") final String coordinate,
      @JsonProperty("timezone") final String timezone,
      @JsonProperty("tzoffset") final Integer tzOffset,
      @JsonProperty("days") final VisualCrossingDay[] days) {
    this.coordinate = new Coordinate(coordinate);
    this.timezone = timezone;
    this.tzOffset = tzOffset;
    this.days = days;
  }
}

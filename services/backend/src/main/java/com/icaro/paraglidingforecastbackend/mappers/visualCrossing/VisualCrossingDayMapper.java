package com.icaro.paraglidingforecastbackend.mappers.visualCrossing;

import com.icaro.paraglidingforecastbackend.dto.VisualCrossing.VisualCrossingDay;
import com.icaro.paraglidingforecastbackend.models.forecast.ForecastDay;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    uses = {VisualCrossingHourMapper.class})
public interface VisualCrossingDayMapper {
  @Mapping(target = "score", ignore = true)
  ForecastDay toForecastDay(VisualCrossingDay visualCrossingDay);
}

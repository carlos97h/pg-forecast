package com.icaro.paraglidingforecastbackend.dto.WeatherApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.joda.time.LocalDate;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class WeatherApiForecastDay {
  private final LocalDate date;
  private final WeatherApiAstro astro;
  private final WeatherApiHour[] hours;

  public WeatherApiForecastDay(
      @JsonProperty("date") final String date,
      @JsonProperty("astro") final WeatherApiAstro astro,
      @JsonProperty("hour") final WeatherApiHour[] hours) {
    this.date = LocalDate.parse(date);
    this.astro = astro;
    this.hours = hours;
  }
}
